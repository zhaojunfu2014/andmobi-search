package com.andmobi.search.cluster.handler;

public class Command {
	/**
	 * 传输日志
	 */
	public static final String LOG= "com.andmobi.search.cluster.handler.LogSendHandler";
	
	/**
	 * 获取状态
	 */
	public static final String HEARTBEAT = "com.andmobi.search.cluster.handler.HeartBeatHandler";
}
