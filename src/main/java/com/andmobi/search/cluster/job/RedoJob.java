package com.andmobi.search.cluster.job;

import java.io.File;
import java.util.TimerTask;

import com.andmobi.search.AndmobiSearcher;
import org.apache.log4j.Logger;

import com.andmobi.search.ConfigHolder;
import com.andmobi.search.cluster.log.LogParser;
import com.andmobi.search.SearchGlobal;
import com.andmobi.search.service.SearchService;
import com.andmobi.search.util.FileUtil;

/**
 * 
 * @Title:RedoJob
 * @description:日志重做任务
 * @author 赵俊夫
 * @date 2014-7-19 上午1:51:20
 * @version V1.0
 */
public class RedoJob extends TimerTask{
	private static final Logger log= Logger.getLogger(RedoJob.class);

	private SearchService searchService = AndmobiSearcher.getService();
	
	private void moveFileToArchiver(File[] subTodos) {
		for(File f:subTodos){
			String filename=f.getName();
			FileUtil.cutGeneralFile(ConfigHolder.logSubTodo+SearchGlobal.pathSeparator+filename, ConfigHolder.logSubArchiver);
		}
	}
	@Override
	public synchronized void run() {
		//将订阅到的日志重做
		File subTodoDir = new File(ConfigHolder.logSubTodo);
		File[] subTodos = subTodoDir.listFiles();
		log.debug(subTodos.length+" logs");
		if(subTodos == null || subTodos.length==0){
			return;
		}
		for(File f:subTodos){
			Object o = LogParser.parseLog(ConfigHolder.logSubTodo, f.getName());
			try {
				searchService.createOrUpdateIndex(o);
			} catch (Exception e) {
				e.printStackTrace();
				log.error(e,e);
			}
		}
		moveFileToArchiver(subTodos);
		log.debug(subTodos.length+" logs finished");
	}
}
