package com.andmobi.junit;



import com.andmobi.search.annotation.SearchColum;
import com.andmobi.search.annotation.SearchEntity;

@SearchEntity(searchFields={"author","content","type"},idFiled="name")
public class SearchTestEntity {
	
	@SearchColum(name="author", index = true, store = true)
	private String author;
	
	@SearchColum(index =true, store = true,highLight = true)
	private String content;
	
	@SearchColum(index =true,name="name", store = true)
	private String name;

    @SearchColum(index =true,name="type", store = true)
    private String type;
	
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SearchTestEntity(String type,String author, String content, String name) {
		super();
        this.type = type;
		this.author = author;
		this.content = content;
		this.name = name;
	}
	public SearchTestEntity() {
		super();
	}

    @Override
    public String toString() {
        return "SearchTestEntity{" +
                "author='" + author + '\'' +
                ", content='" + content + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
