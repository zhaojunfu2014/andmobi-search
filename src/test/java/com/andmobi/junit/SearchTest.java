package com.andmobi.junit;

import com.andmobi.search.AndmobiSearcher;
import com.andmobi.search.pojo.GroupResult;
import com.andmobi.search.pojo.SearchResult;
import com.andmobi.search.service.SearchService;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SearchTest {
    //初始化搜索引擎
	private SearchService service = AndmobiSearcher.getService();

	/**
	 * 创建索引 配置文件：resources/search.properties -indexDir:索引文件存放磁盘路径
     * 其中 name 字段作为索引的 逻辑唯一表示位（主键）
	 */
	@Test
	public void create() {
        List<SearchTestEntity> books = new ArrayList<SearchTestEntity>(10);
        SearchTestEntity s1 = new SearchTestEntity("小说","罗贯中","本书是中国古典四大名著之一，全名为《三国志通俗演义》。作者是元末明初小说家罗贯中，是中国第一部长篇章回体历史演义小说。描写了从东汉末年到西晋初年之间近105年的历史风云。全书反映了三国时代的政治军事斗争，反映了三国时代各类社会矛盾的转化，并概括了这一时代的历史巨变，塑造了一批叱咤风云的三国英雄人物。\n" + "自《三国演义》问世以来，各式各样的版样层出不穷，明代刻本有20多种，清代刻本也有70多种，在中国民间流传甚广。康熙二十八年，日僧湖南文山编译出版日文本《通俗三国志》之后，朝鲜、日本、印度尼西亚、越南、泰国、英国、法国、俄国等许多国家都对《三国演义》有本国文字的译本，并发表了不少研究论文和专著，对这部小说作出了极高的评价。","三国演义");
        SearchTestEntity s2 = new SearchTestEntity("哲学","王阳明","本书是哲学著作，作者是中国明代哲学家、宋明道学中心学一派的代表人物王守仁（字伯安），世称阳明先生。此书记载了他的语录和论学书信。传习一辞源出自《论语》中的传不习乎一语。","传习录");
        SearchTestEntity s3 = new SearchTestEntity("小说","石悦","明朝那些事儿，本书是网络连载的历史小说，作者是当年明月，本名石悦，广东顺德海关公务员。[2] 2006年3月在天涯社区首次发表，2009年3月21日连载完毕，边写作边集结成书出版发行，一共7本[3] 。","明朝那些事儿");
        SearchTestEntity s4 = new SearchTestEntity("传记","陈梧桐","本书是一部人物传记，运用了丰富扎实的史料，生动流畅的文笔，全面记述了中国历史上的杰出政治家和军事家明太祖朱元璋从贫苦出身的小行童到元末农民起义领袖，再到明朝开国皇帝的传奇经历和主要活动，是一部具有重要学术价值和较强可读性的历史著作。","洪武大帝朱元璋传");
        SearchTestEntity s5 = new SearchTestEntity("未知","佚名","本书是一部","不知道");
        books.add(s1);
        books.add(s2);
        books.add(s3);
        books.add(s4);
        books.add(s5);
        try {
            service.createOrUpdateIndexBatch(books);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * 关键字搜索-无分页 关键字搜索支持lucene语法
	 */
	@Test
	public void search() {
		try {
			SearchResult<SearchTestEntity> r = service.findSearchResult(
					com.andmobi.junit.SearchTestEntity.class, "明朝");
			System.out.println(r.getSize());
			for(SearchTestEntity t:r.getResult()){
				System.out.println(t);
			}
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 关键字搜索-分页 关键字搜索支持lucene语法
	 */
	@Test
	public void searchPage() {
		try {
			SearchResult<SearchTestEntity> r = service.findSearchResult(
					com.andmobi.junit.SearchTestEntity.class, "明朝", 1, 1);
			System.out.println(r.getSize());
			for(SearchTestEntity t:r.getResult()){
				System.out.println(t);
			}
		} catch (CorruptIndexException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    /**
     * 分组统计
     * 按照书的类型 统计数量和书名
     */
    @Test
    public void group(){
        try {
            //groupField 分组id,valueField 分组id对应的名称
           GroupResult groupResult = service.group(SearchTestEntity.class, "本书", "type", "name", 10000);
           System.out.println( groupResult.getCountMap());
           //输出结果：{传记=1, 哲学=1, 小说=2, 未知=1}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

	/**
	 * 索引更新
	 */
	@Test
	public void update() {
        SearchTestEntity s5 = new SearchTestEntity("小说","佚名","本书是一部小说","围城");
		try {
			service.createOrUpdateIndex(s5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		search();
	}

	/**
	 * 索引删除
	 */
	@Test
	public void del() {
        SearchTestEntity s5 = new SearchTestEntity();
        s5.setName("围城");
		try {
			service.deleteIndex(s5);
		} catch (Exception e) {
			e.printStackTrace();
		}
		search();
	}

}
